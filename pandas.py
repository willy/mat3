# -*- coding: utf-8 -*-
"""
Created on Fri May 20 18:28:01 2022

@author: wilfr
"""
import numpy as np
import pandas as pd

#%%
# 1.
# Escribe un programa que pregunte al usuario por las ventas de un rango de años y muestre por pantalla una serie con los datos de las ventas
# indexadas por los años, antes y despues de aplicarles un descuento del 10%

#%%
# 2.
# Escribe una funcion que reciba un diccionario con las notas de los alumnos de curso y devuelva una serie con la nota minima, la maxima, media
# y la desviacion tipica de cada uno


#%%
# 3.
# Escribe una funcion que reciba los datos siguientes en un DataFrame, una lista de meses, y devuelva el balance (ventas - gastos) total en los meses
# indicados.

datos = {'Mes' :['Enero', 'Febrero', 'Marzo', 'Abril'], 'Ventas':[30500, 35600, 28300, 33900], 'Gastos': [22000, 23400, 18100, 20700]}                     


#%%
# 4.
# El archivo autos.xlsx contiene datos de precios de autos y stock. Construye el codigo necesario que emita el precio minimo, el maximo y promedio


#%%
# 5.
# El archivo comercio_interno.csv contiene informacion sobre el comercio interno desde la decada del 90. Escribe un programa que:
#   a. Muestre por pantalla las dimensiones del Data Frame, el numero de datos que contiene, los nombres de sus columnas y filas, los tipos de datos de las
#   columnas, las 10 primeras filas y las 10 ultimas filas.

#   b. Muestre por pantalla un grafico de los datos de empleo por provincia y su relacion con la columna valor.

#   c. Muestre por pantalla la columna alcance_nombre ordenada alfabeticamente.

#   d. Muestre un grafico de la actividad_producto_nombre agrupados en relacion al valor

#   e. Sume por alcance_nombre los valores de los años 2009 al 2019

#   f. Muestre un grafico de la actividad_producto_nombre en la provincia de Mendoza del año 2015 al 2019


#%%
# 6.
# La carpeta dataset contiene 3 archivos referentes a usuarios, votos y peliculas:
#   a. Genera el codigo de agrupamiento y agregacion necesario para calcular: suma, cuenta, media, desviacion estandar, utilizando las funciones numpy


#%%
# 7.
# El archivo salarios muestra distintas categorias, antiguedad, salarios, etc:
#   a. Calcula el minimo, maximo y promedio de antiguedad.
#   b. Construye el codigo necesario para eimitir un grafico que muestre los porcentajes de cada cargo.
#   c. Genera el codigo de agrupamiento y agregacion necesario para calcular: suma, media y desviacion estandar, del sario, utilizando las funciones numpy










