"""
En una comunidad de 100 deportistas se sabe que 30 de ellos entrenan futbol, 50 entrenan squash 
y 60 entrenan tenis. 22 entrenan tenis y futbol, 30 entrenan squash y tenis y 15 entrenan squash y futbol. 
Si 10 deportistas entrenan los tres deportes 
1-¿cuantos entrenan solo tenis?
2-¿cuantos entrenan solo futbol?
3-¿cuantos entrenan solo squash?
4-¿cuantos entrenan tenis o futbol?
"""

from matplotlib import pyplot as plt
from matplotlib_venn import venn3, venn3_circles

lista_futbol = [3, 5, 10, 12]
tupla_squash = (10, 20, 15, 5)
diccio_tenis = {"infantil": 12,
                "juniors": 10,
                "adolescentes": 20,
                "adultos": 18}

universal = 100
ninguno = 0

def suma(lista):
    suma = 0
    for elemento in lista:
        suma += elemento
    return suma

futbol = suma(lista_futbol)
print(f"Me dicen que 30 de ellos entrenan futbol, compruebo: {futbol}")

squash = suma(tupla_squash)
print(f"Me dicen que 50 de ellos entrenan squash, compruebo: {squash}")

def suma_diccio(diccio):
    suma = 0
    for elemento in diccio.values():
        suma += elemento
    return suma

tenis = suma_diccio(diccio_tenis)
print(f"Me dicen que 60 de ellos entrenan tenis, compruebo: {tenis}")

###################################################################################
def set_tenis(diccio):
    tenis = set()
    
    for elemento in diccio.values():
        tenis.add(elemento)
    return tenis
#Paso el diccionario de tenis a set
tenis = set_tenis(diccio_tenis)
print(tenis)

#Paso la lista de futbol a set
futbol = set(lista_futbol)
print(futbol)

#Paso la tupla de squash a set
squash = set(tupla_squash)
print(squash)

###################################################################################

#1-¿cuantos entrenan solo tenis?
def soloT( conjunto_Principal, conjuntoAComparar1, conjuntoAComparar2):
    return (conjunto_Principal - conjuntoAComparar1) & (conjunto_Principal - conjuntoAComparar2) 

print(soloT(tenis, futbol, squash))

#2-¿cuantos entrenan solo futbol?
print( soloT(futbol, tenis, squash))

#3-¿cuantos entrenan solo squash?
print( soloT(squash, futbol, tenis))

#4-¿cuantos entrenan tenis o futbol?
def tenis_o_futbol(f, t):
    suma = 0
    
    t_o_f = t | f
    
    for elemento in t_o_f:
        suma = suma + elemento
    return suma

tenis_o_futbol = tenis_o_futbol(futbol, tenis)
print(tenis_o_futbol)

###################################################################################
#22 entrenan tenis y futbol
def soloAyB(tenis, futbol):
    return tenis & futbol

print(f"tenis y futbol: {soloAyB(tenis,futbol)}")

#30 entrenan squash y tenis
print(f"squash y tenis: {soloAyB(squash,tenis)}")

#15 entrenan squash y futbol
print(f"squash y futbol: {soloAyB(squash,futbol)}")

#Si 10 deportitas entrenan los tres deportes
def soloABC(tenis, futbol, squash):
    return (tenis&futbol) & (tenis&squash)
print(f"tenis, futbol y squash: {soloABC(tenis, futbol, squash)}")

###################################################################################

plt.figure("Ejemplo")
diagram = venn3((1, 1, 1, 1, 1, 1, 1), set_labels=("Futbol", "Squash", "Tenis"))

for subset in ("111", "110", "101", "100", "011", "010", "001"):
    diagram.get_label_by_id(subset).set_fontsize(10)

diagram.get_label_by_id('100').set_text(soloT(futbol, tenis, squash))
diagram.get_label_by_id('001').set_text(soloT(tenis, futbol, squash))
diagram.get_label_by_id('010').set_text(soloT(squash, futbol, tenis))

diagram.get_label_by_id('101').set_text(soloAyB(tenis,futbol) - soloABC(tenis, futbol, squash))
diagram.get_label_by_id('011').set_text(soloAyB(tenis,squash) - soloABC(tenis, futbol, squash))
diagram.get_label_by_id('110').set_text(soloAyB(futbol,squash) - soloABC(tenis, futbol, squash))

diagram.get_label_by_id('111').set_text(soloABC(tenis, futbol, squash))

plt.text(-0.90, 0.67,      # Texto y cantidad universal
         f"Universal = {universal}",
         size=10)

plt.axis('on')
plt.title("Deportistas")
plt.show()
