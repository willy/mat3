# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:16:33 2022

@author: wilfr
"""

"""
En una comunidad de 100 deportistas se sabe que 30 de ellos entrenan futbol, 50 entrenan squash 
y 60 entrenan tenis. 22 entrenan tenis y futbol, 30 entrenan squash y tenis y 15 entrenan squash y futbol. 
Si 10 deportistas entrenan los tres deportes 
1-¿cuantos entrenan solo tenis?
2-¿cuantos entrenan solo futbol?
3-¿cuantos entrenan solo squash?
4-¿cuantos entrenan tenis o futbol?
"""

from matplotlib import pyplot as plt
from matplotlib_venn import venn3, venn3_circles

U = 100
lista_futbol = [3, 5, 10, 12]
tupla_squash = (10, 20, 15, 5)
diccio_tenis = {"infantil": 12,
                "juniors": 10,
                "adolescentes": 20,
                "adultos": 18}

universal = 100
ninguno = 0

def suma(lista):
    suma = 0
    for elemento in lista:
        suma += elemento
    return suma

def sumaDiccio(diccio):
    suma = 0
    for elemento in diccio.values():
        suma += elemento
    return suma

futbol = suma(lista_futbol)
print(f"Me dicen que 30 de ellos entrenan futbol, compruebo: {futbol}")

squash = suma(tupla_squash)
print(f"Me dicen que 50 de ellos entrenan squash, compruebo: {squash}")

tenis = sumaDiccio(diccio_tenis)
print(f"Me dicen que 60 de ellos entrenan tenis, compruebo: {tenis}")

###################################################################################

def set_tenis(diccio):
    tenis = set()
    
    for elemento in diccio.values():
        tenis.add(elemento)
    return tenis

tenis = set_tenis(diccio_tenis)
print(tenis)

futbol = set(lista_futbol)
print(futbol)

squash = set(tupla_squash)
print(squash)

###################################################################################

#1-¿cuantos entrenan solo tenis?

def SoloUnDeporte( a,b,c):
    return (a-b) - c
print(f"Cuantos entrenan tenis? : {SoloUnDeporte(tenis,futbol,squash)}")

#2-¿cuantos entrenan solo futbol?
print(f"Cuantos entrenan tenis? : {SoloUnDeporte(futbol,tenis,squash)}")

#3-¿cuantos entrenan solo squash?
print(f"Cuantos entrenan tenis? : {SoloUnDeporte(squash,futbol,tenis)}")

#4-¿cuantos entrenan tenis o futbol?

def TenisOFutbol(a,b):
    suma = 0
    
    tenisOFutbol = a|b
    
    for elemento in tenisOFutbol:
        suma += elemento
    return suma

print(f"Cuantos entrenan Tenis o futbol {TenisOFutbol(tenis,futbol)}")
###################################################################################

#22 entrenan tenis y futbol
def InterseccionAyB(a,b):
    return a&b

print(f"22 entrenan tenis y futbol : {InterseccionAyB(tenis,futbol)}")

#30 entrenan squash y tenis
print(f"30 entrenan squash y tenis : {InterseccionAyB(squash,tenis)}")

#15 entrenan squash y futbol
print(f"15 entrenan squash y futbol : {InterseccionAyB(squash,futbol)}")

#Si 10 deportistas entrenan los tres deportes
def TresDeportes(a,b,c):
    return (a&b) & c

print(f"10 deportistas entren los tres deportes: {TresDeportes(futbol,tenis,squash)}")

###################################################################################
#Grafico

plt.figure("Simulacro 3")
diagram = venn3( (1, 1, 1, 1, 1, 1, 1), set_labels=("Futbol", "Squash", "Tenis"))

for subset in ("111", "110", "101", "100", "011", "010", "001"):
    diagram.get_label_by_id(subset).set_fontsize(10)
    
diagram.get_label_by_id('100').set_text(SoloUnDeporte(futbol,tenis,squash))
diagram.get_label_by_id('110').set_text(InterseccionAyB(futbol,squash))
diagram.get_label_by_id('101').set_text(InterseccionAyB(futbol,tenis))

diagram.get_label_by_id('010').set_text(SoloUnDeporte(squash,tenis,futbol))
diagram.get_label_by_id('011').set_text(InterseccionAyB(squash,tenis))

diagram.get_label_by_id('001').set_text(SoloUnDeporte(tenis,squash,futbol))

diagram.get_label_by_id('111').set_text(TresDeportes(futbol,tenis,squash))

plt.text(-0.90, 0.67,
         f"Universal = {universal}",
         size=10)

plt.axis('on')
plt.title("Deportistas")
plt.show()